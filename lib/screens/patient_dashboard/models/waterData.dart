class WaterData{
  final String consumed;
  final String target;
  final String last_seen;
  final String time;

  WaterData(
      {required this.consumed,required this.target,required this.last_seen,required this.time});

}